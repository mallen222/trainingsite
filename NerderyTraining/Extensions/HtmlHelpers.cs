﻿using System;
using System.Web;
using System.Web.Mvc;

namespace NerderyTraining.Extensions
{
    public static class HtmlHelpers
    {
        public static MvcHtmlString TopicMenuItem(this HtmlHelper htmlHelper, string url, string text)
        {
            var menu = new TagBuilder("li");
            menu.Attributes.Add("role", "presentation");
            menu.Attributes.Add("class", "js-nav-item");
            var currentUrl = HttpContext.Current.Request.Url.PathAndQuery;
            if (string.Equals(
                    currentUrl,
                    url,
                    StringComparison.CurrentCultureIgnoreCase)
            )
            {
                menu.AddCssClass("active");
            }

            menu.InnerHtml = $@"<a class=""js-nav-item-link"" href=""{url}"">{text}</a>";

            return MvcHtmlString.Create(menu.ToString());
        }
    }
}
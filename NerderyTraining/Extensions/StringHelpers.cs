﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace NerderyTraining.Extensions
{
    public static class StringHelpers
    {
        public static IPublishedContent GetContentFromUmbracoId(this string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return null;
            }

            var helper = new UmbracoHelper(UmbracoContext.Current);
            return (IPublishedContent) helper.Content(id);
        }

        public static string GetContentNameFromUmbracoId(this string id)
        {
            var content = GetContentFromUmbracoId(id);
            return (content != null) ? content.Name : string.Empty;
        }
    }
}
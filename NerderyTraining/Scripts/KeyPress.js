﻿document.onkeydown = checkKey;

function prevNav() {
    var targets = $('.js-nav .js-nav-item');
    var activeTarget = $(".js-nav .js-nav-item.active");
    var activeTargetIndex = targets.index(activeTarget);

    if (activeTargetIndex === 0 || activeTargetIndex === -1) {
        window.location.href = "/";
        return;
    }

    var prevTarget = $(targets[activeTargetIndex - 1]);
    var prevLink = prevTarget.find('.js-nav-item-link');
    window.location.href = prevLink.attr("href");
}

function nextNav() {
    var targets = $('.js-nav .js-nav-item');
    var activeTarget = $(".js-nav .js-nav-item.active");
    var activeTargetIndex = targets.index(activeTarget);

    if (activeTargetIndex === targets.length - 1 || activeTargetIndex === -1) {
        window.location.href = "/";
        return;
    }

    var nextTarget = $(targets[activeTargetIndex + 1]);
    var nextLink = nextTarget.find('.js-nav-item-link');
    window.location.href = nextLink.attr("href");
}

function prev() {
    var targets = $('.js-tab-nav');
    var activeTarget = $(".js-tab-nav[data-active='true']");
    var activeTargetIndex = targets.index(activeTarget);

    if (activeTargetIndex === -1) {
        activeTargetIndex = 0;
        activeTarget = $(targets[activeTargetIndex]);
    }

    if (activeTargetIndex === 0) {
        prevNav();
        return;
    }

    var nextTarget = $(targets[activeTargetIndex - 1]);
    activeTarget.removeAttr('data-active');
    nextTarget.attr('data-active', 'true');
    activeTarget.find('.js-prev').click();
}

function next() {
    var nextTargets = $('.js-next');
    if (nextTargets.length === 1) {
        // If there's just one next button, click it
        window.location.href = nextTargets.first().attr("href");
        return;
    }

    var targets = $('.js-tab-nav');
    var activeTarget = $(".js-tab-nav[data-active='true']");
    var activeTargetIndex = targets.index(activeTarget);

    if (activeTargetIndex === targets.length - 1) {
        nextNav();
        return;
    }

    if (activeTargetIndex === -1) {
        activeTargetIndex = 0;
        activeTarget = $(targets[activeTargetIndex]);
    }
    var nextTarget = $(targets[activeTargetIndex + 1]);
    activeTarget.removeAttr('data-active');
    nextTarget.attr('data-active', 'true');
    activeTarget.find('.js-next').click();
}


function checkKey(e) {

    e = e || window.event;

    if ((e.keyCode == '37') || (e.keyCode == '38')) {
        // left or up arrow
        prev();
    }
    else if ((e.keyCode == '39') || (e.keyCode == '40')) {
        // right or down arrow
        next();
    }
}
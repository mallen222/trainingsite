﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NerderyTraining.App_Start
{
    internal class RouteConfig
    {
        /// <summary>
        /// Register Umbraco application routes. 
        /// </summary>
        /// <param name="routes"></param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                "Default Umbraco Route",
                "umbraco/{controller}/{action}",
                new { area = "Umbraco", action = "Index" }
            );
        }
    }
}
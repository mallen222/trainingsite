﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using UmbracoVault;
using UmbracoVault.Attributes;

namespace NerderyTraining.Models
{
    [UmbracoEntity(Alias = "trainingComponentRow")]
    public class TrainingComponentRowViewModel
    {
        public IPublishedContent Content { get; set; }

        public string ColumnClass => $"col-md-{ 12/TrainingComponents.Count(c => !c.HideForReference) }";

        public IEnumerable<TrainingComponentViewModel> TrainingComponents => 
            Vault.Context.GetChildren<TrainingComponentViewModel>(Content.Id).ToList();
    }
}
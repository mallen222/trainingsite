﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using UmbracoVault.Attributes;

namespace NerderyTraining.Models
{
    [UmbracoEntity(Alias = "home")]
    public class HomeViewModel
    {
        public IPublishedContent Content { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using UmbracoVault.Attributes;

namespace NerderyTraining.Models
{
    [UmbracoEntity(Alias = "genericHTMLPage")]
    public class GenericHTMLPageViewModel
    {
        public IPublishedContent Content { get; set; }
        [UmbracoRichTextProperty]
        public string HtmlContent { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using UmbracoVault;
using UmbracoVault.Attributes;

namespace NerderyTraining.Models
{
    [UmbracoEntity(Alias = "trainingTopic")]
    public class TrainingTopicViewModel
    {
        public IPublishedContent Content { get; set; }

        [UmbracoRichTextProperty]
        public string JumbotronContent { get; set; }

        public IEnumerable<TrainingComponentRowViewModel> Rows => 
            Vault.Context.GetChildren<TrainingComponentRowViewModel>(Content.Id).ToList();
    }
}
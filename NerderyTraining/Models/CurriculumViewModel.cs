﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using UmbracoVault;
using UmbracoVault.Attributes;

namespace NerderyTraining.Models
{
    [UmbracoEntity(Alias = "curriculum", AutoMap = true)]
    public class CurriculumViewModel
    {
        public IPublishedContent Content { get; set; }
        public string Tagline { get; set; }
        public bool IncludeClassroomTraining { get; set; }
        
        public IEnumerable<TrainingModuleViewModel> Modules => 
            Vault.Context.GetChildren<TrainingModuleViewModel>(Content.Id);
    }
}
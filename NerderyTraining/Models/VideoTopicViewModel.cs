﻿using Umbraco.Core.Models;
using UmbracoVault.Attributes;

namespace NerderyTraining.Models
{
    [UmbracoEntity(Alias = "videoTopic")]
    public class VideoTopicViewModel
    {
        public IPublishedContent Content { get; set; }
        [UmbracoProperty]
        public string VimeoId { get; set; }
    }
}
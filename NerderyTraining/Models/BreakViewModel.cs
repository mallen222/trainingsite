﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using UmbracoVault;
using UmbracoVault.Attributes;

namespace NerderyTraining.Models
{
    [UmbracoEntity(Alias = "break")]
    public class BreakViewModel
    {
        public IPublishedContent Content { get; set; }

        [UmbracoProperty]
        public int Time { get; set; }
    }
}
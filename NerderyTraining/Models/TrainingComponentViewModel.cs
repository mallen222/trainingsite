﻿using System.Web.UI.WebControls;
using Umbraco.Core.Models;
using UmbracoVault;
using UmbracoVault.Attributes;
using NerderyTraining.Extensions;

namespace NerderyTraining.Models
{
    [UmbracoEntity(Alias = "trainingComponent")]
    public class TrainingComponentViewModel
    {
        public IPublishedContent Content { get; set; }
        [UmbracoProperty]
        public bool HideHeader { get; set; }
        [UmbracoProperty]
        public string HeaderColor { get; set; }
        [UmbracoProperty]
        public string HeaderSize { private get; set; }
        [UmbracoProperty]
        public string HeaderAlignment { private get; set; }
        [UmbracoProperty]
        public string BorderStyle { private get; set; }
        [UmbracoRichTextProperty]
        public string ReferenceContent { get; set; }
        [UmbracoRichTextProperty]
        public string ClassroomContent { get; set; }
        [UmbracoProperty]
        public bool HideForClassroom { get; set; }
        [UmbracoProperty]
        public bool HideForReference { get; set; }

        public string HeaderSizeClass => HeaderSize.GetContentNameFromUmbracoId();
        public string HeaderAlignmentClass => HeaderAlignment.GetContentNameFromUmbracoId();
        public string BorderClass => BorderStyle.GetContentNameFromUmbracoId();

    }
}
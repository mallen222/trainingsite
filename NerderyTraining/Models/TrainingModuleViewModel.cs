﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using UmbracoVault;
using UmbracoVault.Attributes;

namespace NerderyTraining.Models
{
    [UmbracoEntity(Alias = "trainingModule")]
    public class TrainingModuleViewModel
    {
        public IPublishedContent Content { get; set; }
        [UmbracoRichTextProperty]
        public string Description { get; set; }
        [UmbracoProperty]
        public string Color { get; set; }
        [UmbracoProperty]
        public string TrainingLinkTextOverride { get; set; }
        [UmbracoRichTextProperty]
        public string JumbotronContent { get; set; }
        [UmbracoRichTextProperty]
        public string IntroText { get; set; }

        public IEnumerable<IPublishedContent> Topics => Content.Children;

        [UmbracoIgnoreProperty]
        public string TrainingLinkText => 
            (!string.IsNullOrWhiteSpace(TrainingLinkTextOverride))
                ? TrainingLinkTextOverride
                : "Start Training";
    }
}
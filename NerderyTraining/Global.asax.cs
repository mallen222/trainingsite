﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using NerderyTraining.App_Start;
using NerderyTraining.Controllers;
using Umbraco.Core;
using Umbraco.Core.Security;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using UmbracoVault;
using UmbracoVault.Caching;
using UmbracoVault.Controllers;

namespace NerderyTraining
{
    public class CustomApplicationEventHandler : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            Vault.RegisterViewModelNamespace("NerderyTraining.Models", "NerderyTraining");
            DefaultRenderMvcControllerResolver.Current.SetDefaultControllerType(typeof(BaseController));
        }
    }
}
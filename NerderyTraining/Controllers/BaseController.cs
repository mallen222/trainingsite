﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using UmbracoVault;
using UmbracoVault.Controllers;
using UmbracoVault.Exceptions;

namespace NerderyTraining.Controllers
{
    public class BaseController : RenderMvcController
    {
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            // Get IsClass cookie
            ViewBag.IsClass = (Request.Cookies["isClass"] != null && string.Compare(Request.Cookies["isClass"].Value, "true", StringComparison.InvariantCultureIgnoreCase) == 0);

            // Set curriculum content viewbag
            var helper = new UmbracoHelper(UmbracoContext.Current);
            var currentContent = (IPublishedContent)helper.Content(UmbracoContext.Current.PageId);
            while (currentContent.DocumentTypeAlias != "curriculum" && currentContent.DocumentTypeAlias != "Home")
            {
                currentContent = currentContent.Parent;
            }
            ViewBag.CurrentCurriculum = currentContent;
        }

        public BaseController()
        {
        }

        public BaseController(UmbracoContext umbracoContext) : base(umbracoContext)
        {
        }

        /// <summary>
        ///     The default action to render the front-end view
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override ActionResult Index(RenderModel model)
        {
            return CurrentTemplate(model);
        }

        /// <summary>
        ///     Returns an ActionResult based on the template name found in the route values and the given model.
        /// </summary>
        /// <returns></returns>
        protected new ActionResult CurrentTemplate<T>(T model)
        {
            var template = ControllerContext.RouteData.Values["action"].ToString();
            var viewName = (ViewBag.IsClass) ? $"{template}_class" : template;

            var checkedTypes = new List<string>();
            //Vault.RegisterViewModelNamespace("NerderyTraining.Models", "NerderyTraining");
            var viewModelTypeString = $"NerderyTraining.Models.{template}ViewModel,NerderyTraining";
            checkedTypes.Add(viewModelTypeString);
            var type = Type.GetType(viewModelTypeString);
            if (type != null)
            {
                var inferredViewModel = Vault.Context.GetCurrent(type);
                if (inferredViewModel != null)
                {
                    return View(viewName, inferredViewModel);
                }
            }

            if (model == null)
            {
                throw new ViewModelNotFoundException(template, checkedTypes);
            }

            // Look for different view for classroom training. 
            // All classroom views will use the convention of  [templateName]_class.cshtml
            

            return View(viewName, model);
        }

        private ActionResult CurrentTemplate()
        {
            return CurrentTemplate<object>(null);
        }

    }
}